# Selwiak Przemyslaw 2021

import serial
from time import sleep
from tkinter import *
from tkinter.ttk import *

Message = bytearray([0, 0, 0, 0, 0, 0, 0, 0])

ser = serial.Serial(
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1,
    port=None
)

def portError():
    newWindow=Toplevel(root)
    newWindow.title("Error")
    newWindow.geometry('200x50')
    newWindow.resizable(width=False, height=False)
    newWindow = Label(newWindow, text="Wrong COM port selected", anchor=CENTER)
    newWindow.pack()

def sendMessage():
    Message[7] = varb7.get()
    Message[6] = varb6.get()
    Message[5] = varb5.get()
    Message[4] = varb4.get()
    Message[3] = varb3.get()
    Message[2] = varb2.get()
    Message[1] = varb1.get()
    Message[0] = varb0.get()

    try:
        ser = serial.Serial(
            port=port.get()
        )
    except serial.serialutil.SerialException:
        portError()

    if ser.isOpen():
        ser.write(Message)

    counter = 0

    while repeating.get():
        if counter < frames.get():
            ser.write(Message)
            counter = counter + 1
            sleep((period.get()/1000))
        else:
            break



root = Tk()
root.title("USB - SPI terminal")
root.geometry('385x130')
root.resizable(width=False, height=False)

l0 = Label(root)
l0.grid(column=0, row=0)

varb7 = IntVar(root)
varb7.set(0)
b7 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb7)
b7.grid(column=1, row=1)

varb6 = IntVar(root)
varb6.set(0)
b6 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb6)
b6.grid(column=2, row=1)

varb5 = IntVar(root)
varb5.set(0)
b5 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb5)
b5.grid(column=3, row=1)

varb4 = IntVar(root)
varb4.set(0)
b4 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb4)
b4.grid(column=4, row=1)

varb3 = IntVar(root)
varb3.set(0)
b3 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb3)
b3.grid(column=5, row=1)

varb2 = IntVar(root)
varb2.set(0)
b2 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb2)
b2.grid(column=6, row=1)

varb1 = IntVar(root)
varb1.set(0)
b1 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb1)
b1.grid(column=7, row=1)

varb0 = IntVar(root)
varb0.set(0)
b0 = Spinbox(root, values=(0, 1), width=3, state='readonly', textvariable=varb0)
b0.grid(column=8, row=1)

l01 = Label(root)
l01.grid(column=9, row=1)

przycisk = Button(root, text="Send", command=sendMessage)
przycisk.grid(column=10, row=1)

l1 = Label(root)
l1.grid(column=0, row=2)

port=StringVar(root)
COMport = Combobox(root, textvariable=port, width=9)
COMport['values'] = ('COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6', 'COM7', 'COM8')
COMport.current(0)
COMport.grid(column=1, row=3, columnspan=2)


def Validation(S):
    if S in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        return True
    root.bell()  # .bell() plays that ding sound telling if there was invalid input
    return False

vcmd = (root.register(Validation), '%S')

def limitSize(*args):
    value = period.get()
    if len(value) > 5: period.set(value[:5])

period=IntVar(root)
periodTime = Combobox(root, textvariable=period, width=9)
periodTime['values'] = (500, 1000)
periodTime.current(0)
periodTime.grid(column=3, row=3, columnspan=3)

ms = Label(root, text="ms")
ms.grid(column=4, row=3)

frames = IntVar(root)
frames.set(1)
b1 = Spinbox(root, from_=0, to_=100, state='readonly', width=6, textvariable=frames)
b1.grid(column=6, row=3, columnspan=2)

repeating = BooleanVar()
repeat = BooleanVar()
repeat.set(False)  # ustawienie True= zakreślone a Fałsz=puste okienko
repeat = Checkbutton(root, text='Periodic sending', var=repeating)
repeat.grid(column=8, row=3, columnspan=5)

l1 = Label(root)
l1.grid(column=0, row=4)

footer = Label(root, text="Selwiak Przemyslaw 2021")
footer.grid(column=4, row=5, columnspan=4)

root.mainloop()
